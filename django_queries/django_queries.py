class Item:
    def __init__(self, name: str, price: float, category: str):
        self.name = name
        self.price = price
        self.category = category

    def __str__(self):
        return self.name + '@' + str(self.price) + '-' + self.category


class Query:
    def __init__(self, field, operation, value):
        self.field = field
        self.operation = operation
        self.value = value


class Store:
    def __init__(self):
        self.items = []

    def add_item(self, item: Item):
        self.items.append(item)

    def remove_item(self, item: Item):
        self.items.remove(item)

    def __str__(self):
        store_items = [each_item for each_item in self.items]
        return ", ".join([str(item) for item in store_items])

    def filter(self, *args: Query):

        for query in args:
            equal_items = Store()
            if query.operation == 'EQ':
                for each_item in self.items:
                    if query.field == "name" and each_item.name == query.value:
                        equal_items.add_item(each_item)
                    if query.field == "price" and each_item.price == query.value:
                        equal_items.add_item(each_item)
                    if query.field == "category" and each_item.category == \
                            query.value:
                        equal_items.add_item(each_item)
                return equal_items

            in_items = Store()
            if query.operation == 'IN':
                for each_item in self.items:
                    if query.field == "name" and each_item.name in query.value:
                        in_items.add_item(each_item)
                    if query.field == "price" and each_item.price in query.value:
                        in_items.add_item(each_item)
                    if query.field == "category" and each_item.category in \
                            query.value:
                        in_items.add_item(each_item)
                return in_items

            greater_than_items = Store()
            if query.operation == 'GT':
                for each_item in self.items:
                    if query.field == "name" and each_item.name > query.value:
                        greater_than_items.add_item(each_item)
                    if query.field == "price" and each_item.price > query.value:
                        greater_than_items.add_item(each_item)
                    if query.field == "category" and each_item.category > query.value:
                        greater_than_items.add_item(each_item)

                return greater_than_items

            greater_than_eq_items = Store()
            if query.operation == 'GTE':
                for each_item in self.items:
                    if query.field == "name" and each_item.name >= query.value:
                        greater_than_eq_items.add_item(each_item)
                    if query.field == "price" and each_item.price >= query.value:
                        greater_than_eq_items.add_item(each_item)
                    if query.field == "category" and each_item.category >= \
                            query.value:
                        greater_than_eq_items.add_item(each_item)
                return greater_than_eq_items

            less_than_items = Store()
            if query.operation == 'LT':
                for each_item in self.items:
                    if query.field == "name" and each_item.name < query.value:
                        less_than_items.add_item(each_item)
                    if query.field == "price" and each_item.price < query.value:
                        less_than_items.add_item(each_item)
                    if query.field == "category" and each_item.category < \
                            query.value:
                        less_than_items.add_item(each_item)
                return less_than_items

            less_than_eq_items = Store()
            if query.operation == 'LTE':
                for each_item in self.items:
                    if query.field == "name" and each_item.name <= query.value:
                        less_than_eq_items.add_item(each_item)
                    if query.field == "price" and each_item.price <= query.value:
                        less_than_eq_items.add_item(each_item)
                    if query.field == "category" and each_item.category <= \
                            query.value:
                        less_than_eq_items.add_item(each_item)
                return less_than_eq_items

            starts_with_items = Store()
            if query.operation == "starts_with":
                for each_item in self.items:
                    if query.field == "name" and each_item.name.startswith(
                            query.value):
                        starts_with_items.add_item(each_item)
                    if query.field == "price" and each_item.price.startswith(
                            query.value):
                        starts_with_items.add_item(each_item)
                    if query.field == "category" and each_item.category.startswith(
                            query.value):
                        starts_with_items.add_item(each_item)
                return starts_with_items

            ends_with_items = Store()
            if query.operation == "ends_with":
                for each_item in self.items:
                    if query.field == "name" and each_item.name.endswith(
                            query.value):
                        ends_with_items.add_item(each_item)
                    if query.field == "price" and each_item.price.endswith(
                            query.value):
                        ends_with_items.add_item(each_item)
                    if query.field == "category" and each_item.category.endswith(
                            query.value):
                        ends_with_items.add_item(each_item)
                return ends_with_items

            contains_items = Store()
            if query.operation == "contains":
                for each_item in self.items:
                    field_value = self.get_value_for_field(each_item, query.field)
                    if query.value in field_value:
                        contains_items.add_item(each_item)
                return contains_items

    def get_value_for_field(self, item, field):
        if field == "name":
            return item.name
        if field == "price":
            return item.price
        if field == "category":
            return item.category


    def exclude(self, query: Query):
        all_items = self.items
        equal_items = Store()
        if query.operation == 'EQ':
            for each_item in all_items:
                if query.field == "name" and each_item.name not in query.value:
                    equal_items.add_item(each_item)
                if query.field == "price" and each_item.price not in \
                        query.value:
                    equal_items.add_item(each_item)
                if query.field == "category" and each_item.category not in \
                        query.value:
                    equal_items.add_item(each_item)
            return equal_items

        in_items = Store()
        if query.operation == 'IN':
            for each_item in self.items:
                if query.field == "name" and each_item.name not in query.value:
                    in_items.add_item(each_item)
                if query.field == "price" and each_item.price not in \
                        query.value:
                    in_items.add_item(each_item)
                if query.field == "category" and each_item.category not in \
                        query.value:
                    in_items.add_item(each_item)
            return in_items

        greater_than_items = Store()
        if query.operation == 'GT':
            for each_item in self.items:
                if query.field == "name" and each_item.name <= query.value:
                    greater_than_items.add_item(each_item)
                if query.field == "price" and each_item.price <= query.value:
                    greater_than_items.add_item(each_item)
                if query.field == "category" and each_item.category <= \
                        query.value:
                    greater_than_items.add_item(each_item)

            return greater_than_items

        greater_than_eq_items = Store()
        if query.operation == 'GTE':
            for each_item in self.items:
                if query.field == "name" and each_item.name < query.value:
                    greater_than_eq_items.add_item(each_item)
                if query.field == "price" and each_item.price < query.value:
                    greater_than_eq_items.add_item(each_item)
                if query.field == "category" and each_item.category < \
                        query.value:
                    greater_than_eq_items.add_item(each_item)
            return greater_than_eq_items

        less_than_items = Store()
        if query.operation == 'LT':
            for each_item in self.items:
                if query.field == "name" and each_item.name >= query.value:
                    less_than_items.add_item(each_item)
                if query.field == "price" and each_item.price >= query.value:
                    less_than_items.add_item(each_item)
                if query.field == "category" and each_item.category >= \
                        query.value:
                    less_than_items.add_item(each_item)
            return less_than_items

        less_than_eq_items = Store()
        if query.operation == 'LTE':
            for each_item in self.items:
                if query.field == "name" and each_item.name > query.value:
                    less_than_eq_items.add_item(each_item)
                if query.field == "price" and each_item.price > query.value:
                    less_than_eq_items.add_item(each_item)
                if query.field == "category" and each_item.category > \
                        query.value:
                    less_than_eq_items.add_item(each_item)
            return less_than_eq_items

        starts_with_items = Store()
        if query.operation == "starts_with":
            for each_item in self.items:
                if query.field == "name" and not each_item.name.startswith(
                        query.value):
                    starts_with_items.add_item(each_item)
                if query.field == "price" and not each_item.price.startswith(
                        query.value):
                    starts_with_items.add_item(each_item)
                if query.field == "category" and \
                        not each_item.category.startswith(
                            query.value):
                    starts_with_items.add_item(each_item)
            return starts_with_items

        ends_with_items = Store()
        if query.operation == "ends_with":
            for each_item in self.items:
                if query.field == "name" and not each_item.name.endswith(
                        query.value):
                    ends_with_items.add_item(each_item)
                if query.field == "price" and not each_item.price.endswith(
                        query.value):
                    ends_with_items.add_item(each_item)
                if query.field == "category" and \
                        not each_item.category.endswith(
                            query.value):
                    ends_with_items.add_item(each_item)
            return ends_with_items

        contains_items = Store()
        if query.operation == "contains":
            for each_item in self.items:
                if query.field == "name" and query.value not in \
                        each_item.name:
                    contains_items.add_item(each_item)
                if query.field == "price" and query.value not in \
                        each_item.price:
                    contains_items.add_item(each_item)
                if query.field == "category" and query.value not in \
                        each_item.category:
                    contains_items.add_item(each_item)
            return contains_items

    def __add__(self, other):
        return self or other

    def count(self):
        return len(self.items)
