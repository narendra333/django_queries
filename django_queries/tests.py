from django_queries.django_queries import Item, Store, Query


def test_str_for_item_class():
    item = Item(name="biscuit", price=0, category="eatables")
    assert str(item) == "biscuit@0-eatables"


def test_add_item_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    assert str(store) == 'biscuit@0-eatables, chocolate@100-eatables'


def test_filter_eq_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    query = Query(field="name", operation="EQ", value="chocolate")
    store = store.filter(query)
    assert str(store) == 'chocolate@100-eatables'


def test_filter_in_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    item3 = Item(name="ice_cream", price=50, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    store.add_item(item3)
    query = Query(field="price", operation="IN", value=[0, 50])
    store = store.filter(query)
    assert str(store) == "biscuit@0-eatables, ice_cream@50-eatables"


def test_filter_gt_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    query = Query(field="price", operation="GT", value=40)
    store = store.filter(query)

    assert str(store) == "chocolate@100-eatables"


def test_filter_gte_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    query = Query(field="price", operation="GTE", value=100)
    store = store.filter(query)
    assert str(store) == "chocolate@100-eatables"


def test_filter_lt_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    query = Query(field="price", operation="LT", value=50)
    store = store.filter(query)
    assert str(store) == "biscuit@0-eatables"


def test_filter_lte_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    query = Query(field="price", operation="LTE", value=0)
    store = store.filter(query)
    assert str(store) == "biscuit@0-eatables"


def test_filter_starts_with_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    query = Query(field="name", operation="starts_with", value="choc")
    store = store.filter(query)
    assert str(store) == "chocolate@100-eatables"


def test_filter_ends_with_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    query = Query(field="name", operation="ends_with", value="ate")
    store = store.filter(query)
    assert str(store) == "chocolate@100-eatables"


def test_filter_contains_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    query = Query(field="name", operation="contains", value="t")
    store = store.filter(query)
    assert str(store) == "biscuit@0-eatables, chocolate@100-eatables"


def test_exclude_eq_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    query = Query(field="name", operation="EQ", value="chocolate")
    store = store.exclude(query)
    assert str(store) == "biscuit@0-eatables"


def test_exclude_gt_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    query = Query(field="price", operation="GT", value=40)
    item = store.exclude(query)

    assert str(item) == "biscuit@0-eatables"


def test_exclude_gte_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    query = Query(field="price", operation="GTE", value=100)
    item = store.exclude(query)
    assert str(item) == "biscuit@0-eatables"


def test_exclude_lt_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    query = Query(field="price", operation="LT", value=50)
    item = store.exclude(query)
    assert str(item) == "chocolate@100-eatables"


def test_exclude_lte_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    query = Query(field="price", operation="LTE", value=0)
    item = store.exclude(query)
    assert str(item) == "chocolate@100-eatables"


def test_exclude_starts_with_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    query = Query(field="name", operation="starts_with", value="choc")
    item = store.exclude(query)
    assert str(item) == "biscuit@0-eatables"


def test_exclude_ends_with_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    query = Query(field="name", operation="ends_with", value="ate")
    item = store.exclude(query)
    assert str(item) == "biscuit@0-eatables"


def test_exclude_contains_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    query = Query(field="name", operation="contains", value="hoco")
    item = store.exclude(query)
    assert str(item) == "biscuit@0-eatables"


def test_exclude_in_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    item3 = Item(name="ice_cream", price=50, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    store.add_item(item3)
    query = Query(field="price", operation="IN", value=[0, 50])
    item = store.exclude(query)
    assert str(item) == 'chocolate@100-eatables'


def test_add_two_filter_method():
    item1 = Item(name="chocolate", price=100, category="eatables")
    item2 = Item(name="ice_cream", price=50, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    query1 = Query(field="price", operation="EQ", value=100)
    query2 = Query(field="price", operation="EQ", value=40)
    item = store.filter(query1) + store.filter(query2)
    assert str(item) == 'chocolate@100-eatables'


def test_count_method():
    item1 = Item(name="biscuit", price=0, category="eatables")
    item2 = Item(name="chocolate", price=100, category="eatables")
    item3 = Item(name="ice_cream", price=50, category="eatables")
    store = Store()
    store.add_item(item1)
    store.add_item(item2)
    store.add_item(item3)
    query = Query(field="price", operation="IN", value=[0, 100])
    results = store.filter(query)
    count = results.count()
    assert count == 2
